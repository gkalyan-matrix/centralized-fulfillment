/*
 * Copyright (c) 2020, Matrix Absence Management, Inc. All rights reserved.
 * 
 */
package com.matrixcos.backendprocess.cf.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;

/**
 * The Class BackendprocessCfProperties.
 */
public class BackendprocessCfProperties {

    /** The logger. */
    protected static Logger logger = ESAPI.getLogger(BackendprocessCfProperties.class);

    /** The backendprocess cf properties. */
    private static volatile BackendprocessCfProperties backendprocessCfProperties;

    /** The props. */
    private Properties props;

    /**
     * Instantiates a new backendprocess cf properties.
     */
    public BackendprocessCfProperties() {
        props = new Properties();
        InputStream fis = null;
        try {
            fis = this.getClass().getClassLoader().getResourceAsStream("backendprocess-cf.properties");
            props.load(fis);
        } catch (final Exception e) {
            logger.info(Logger.EVENT_FAILURE, "Can not read backendprocess-cf.properties file" + e.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (final IOException e) {
                    logger.info(Logger.EVENT_FAILURE, "Can not close input stream" + e.getMessage());
                }
            }
        }
    }

    /**
     * Gets the single instance of BackendprocessCfProperties.
     *
     * @return single instance of BackendprocessCfProperties
     */
    public static synchronized BackendprocessCfProperties getInstance() {
        if (backendprocessCfProperties == null) {
            backendprocessCfProperties = new BackendprocessCfProperties();
        }
        return backendprocessCfProperties;
    }

    /**
     * Gets the backendprocess cf properties.
     *
     * @return the backendprocess cf properties
     */
    public Properties getBackendprocessCfProperties() {
        return props;
    }

    /**
     * Gets the backendprocess cf property.
     *
     * @param key
     *            the key
     * @return the backendprocess cf property
     */
    public String getBackendprocessCfProperty(final String key) {
        return props.getProperty(key);
    }
}
