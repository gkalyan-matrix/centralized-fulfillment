/*
 * Copyright (c) 2020, Matrix Absence Management, Inc. All rights reserved.
 * 
 */
package com.matrixcos.backendprocess.cf.model.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.matrixcos.db.lib.model.Persistent;

/**
 * The Class PacsDocumentsMetadata.
 */
@Entity
@Table(name = "PACS_DOCUMENTS_METADATA",
        schema = "MATEREP")
public class PacsDocumentsMetadata implements Persistent {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4040945380187058745L;
    
    /** The Constant COLUMN_LENGTH_10. */
    private static final int COLUMN_LENGTH_10 = 10;
    
    /** The Constant COLUMN_LENGTH_25. */
    private static final int COLUMN_LENGTH_25 = 25;
    
    /** The Constant COLUMN_LENGTH_40. */
    private static final int COLUMN_LENGTH_40 = 40;
    
    /** The Constant COLUMN_LENGTH_50. */
    private static final int COLUMN_LENGTH_50 = 50;
    
    /** The Constant COLUMN_LENGTH_60. */
    private static final int COLUMN_LENGTH_60 = 60;
    
    /** The Constant COLUMN_LENGTH_100. */
    private static final int COLUMN_LENGTH_100 = 100;
    
    /** The Constant COLUMN_LENGTH_200. */
    private static final int COLUMN_LENGTH_200 = 200;

    /** The document file name. */
    private String documentFileName;

    /** The idx seq. */
    private Integer idxSeq;

    /** The progress recid. */
    private Integer progressRecid;

    /** The doc id. */
    private String docId;

    /** The doc req desc. */
    private String docReqDesc;

    /** The doc client id. */
    private String docClientId;

    /** The doc pdf form send. */
    private String docPdfFormSend;

    /** The last name. */
    private String lastName;

    /** The first name. */
    private String firstName;

    /** The recipient. */
    private String recipient;

    /** The claim number. */
    private String claimNumber;

    /** The intake number. */
    private String intakeNumber;

    /** The matrix client id. */
    private Integer matrixClientId;

    /** The data group. */
    private String dataGroup;

    /** The folder. */
    private String folder;

    /** The doc request id. */
    private String docRequestId;

    /** The requested on. */
    private Date requestedOn;

    /** The requested user. */
    private String requestedUser;

    /** The action. */
    private String action;

    /** The exfield 1. */
    private String exfield1;

    /** The exfield 2. */
    private String exfield2;

    /** The record created date. */
    private Date recordCreatedDate;

    /** The record updated date. */
    private Date recordUpdatedDate;

    /** The java doc nfs reorg done flag. */
    private String javaDocNfsReorgDoneFlag;

    /** The java doc nfs file location. */
    private String javaDocNfsFileLocation;

    /** The java reorg process comments. */
    private String javaReorgProcessComments;

    /** The received date. */
    private String receivedDate;

    /**
     * Gets the received date.
     *
     * @return the received date
     */
    @JsonProperty
    @Transient
    public String getReceivedDate() {
        return receivedDate;
    }

    /**
     * Sets the received date.
     *
     * @param receivedDate
     *            the new received date
     */
    public void setReceivedDate(final String receivedDate) {
        this.receivedDate = receivedDate;
    }

    /**
     * Instantiates a new pacs documents metadata.
     */
    public PacsDocumentsMetadata() {
    }

    /**
     * Instantiates a new pacs documents metadata.
     *
     * @param docReqDesc
     *            the doc req desc
     * @param docId
     *            the doc id
     * @param documentFileName
     *            the document file name
     * @param recordCreatedDate
     *            the record created date
     */
    public PacsDocumentsMetadata(final String docReqDesc, final String docId, final String documentFileName, final Date recordCreatedDate) {
        super();
        this.docReqDesc = docReqDesc;
        this.docId = docId;
        this.documentFileName = documentFileName;
        this.recordCreatedDate = recordCreatedDate;
    }

    /**
     * Instantiates a new pacs documents metadata.
     *
     * @param docReqDesc
     *            the doc req desc
     * @param docId
     *            the doc id
     * @param documentFileName
     *            the document file name
     * @param recordCreatedDate
     *            the record created date
     * @param javaDocNfsFileLocation
     *            the java doc nfs file location
     */
    public PacsDocumentsMetadata(final String docReqDesc, final String docId, final String documentFileName, final Date recordCreatedDate, final String javaDocNfsFileLocation) {
        super();
        this.docReqDesc = docReqDesc;
        this.docId = docId;
        this.documentFileName = documentFileName;
        this.recordCreatedDate = recordCreatedDate;
        this.javaDocNfsFileLocation = javaDocNfsFileLocation;
    }

    /**
     * Instantiates a new pacs documents metadata.
     *
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     * @param intakeNumber
     *            the intake number
     * @param docReqDesc
     *            the doc req desc
     * @param docId
     *            the doc id
     * @param documentFileName
     *            the document file name
     * @param recordCreatedDate
     *            the record created date
     */
    public PacsDocumentsMetadata(final String firstName, final String lastName, final String intakeNumber, final String docReqDesc, final String docId, final String documentFileName, final Date recordCreatedDate) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.intakeNumber = intakeNumber;
        this.docReqDesc = docReqDesc;
        this.docId = docId;
        this.documentFileName = documentFileName;
        this.recordCreatedDate = recordCreatedDate;
    }

    /**
     * Instantiates a new pacs documents metadata.
     *
     * @param documentFileName
     *            the document file name
     * @param idxSeq
     *            the idx seq
     * @param progressRecid
     *            the progress recid
     * @param docId
     *            the doc id
     * @param docReqDesc
     *            the doc req desc
     * @param docClientId
     *            the doc client id
     * @param docPdfFormSend
     *            the doc pdf form send
     * @param lastName
     *            the last name
     * @param firstName
     *            the first name
     * @param recipient
     *            the recipient
     * @param claimNumber
     *            the claim number
     * @param intakeNumber
     *            the intake number
     * @param matrixClientId
     *            the matrix client id
     * @param dataGroup
     *            the data group
     * @param folder
     *            the folder
     * @param docRequestId
     *            the doc request id
     * @param requestedOn
     *            the requested on
     * @param requestedUser
     *            the requested user
     * @param action
     *            the action
     * @param exfield1
     *            the exfield 1
     * @param exfield2
     *            the exfield 2
     * @param recordCreatedDate
     *            the record created date
     * @param recordUpdatedDate
     *            the record updated date
     * @param javaDocNfsReorgDoneFlag
     *            the java doc nfs reorg done flag
     * @param javaDocNfsFileLocation
     *            the java doc nfs file location
     * @param javaReorgProcessComments
     *            the java reorg process comments
     */
 // SUPPRESS CHECKSTYLE 1
    public PacsDocumentsMetadata(final String documentFileName, final Integer idxSeq, final Integer progressRecid, final String docId, final String docReqDesc, final String docClientId, final String docPdfFormSend, final String lastName,
            final String firstName, final String recipient, final String claimNumber, final String intakeNumber, final Integer matrixClientId, final String dataGroup, final String folder, final String docRequestId, final Date requestedOn,
            final String requestedUser, final String action, final String exfield1, final String exfield2, final Date recordCreatedDate, final Date recordUpdatedDate, final String javaDocNfsReorgDoneFlag, final String javaDocNfsFileLocation,
            final String javaReorgProcessComments) {
        super();
        this.documentFileName = documentFileName;
        this.idxSeq = idxSeq;
        this.progressRecid = progressRecid;
        this.docId = docId;
        this.docReqDesc = docReqDesc;
        this.docClientId = docClientId;
        this.docPdfFormSend = docPdfFormSend;
        this.lastName = lastName;
        this.firstName = firstName;
        this.recipient = recipient;
        this.claimNumber = claimNumber;
        this.intakeNumber = intakeNumber;
        this.matrixClientId = matrixClientId;
        this.dataGroup = dataGroup;
        this.folder = folder;
        this.docRequestId = docRequestId;
        this.requestedOn = requestedOn;
        this.requestedUser = requestedUser;
        this.action = action;
        this.exfield1 = exfield1;
        this.exfield2 = exfield2;
        this.recordCreatedDate = recordCreatedDate;
        this.recordUpdatedDate = recordUpdatedDate;
        this.javaDocNfsReorgDoneFlag = javaDocNfsReorgDoneFlag;
        this.javaDocNfsFileLocation = javaDocNfsFileLocation;
        this.javaReorgProcessComments = javaReorgProcessComments;
    }

    /**
     * Gets the document file name.
     *
     * @return the document file name
     */
    @Id
    @Column(name = "DOCUMENT_FILE_NAME",
            length = COLUMN_LENGTH_100)
    @JsonProperty
    public String getDocumentFileName() {
        return documentFileName;
    }

    /**
     * Sets the document file name.
     *
     * @param documentFileName
     *            the new document file name
     */
    public void setDocumentFileName(final String documentFileName) {
        this.documentFileName = documentFileName;
    }

    /**
     * Gets the idx seq.
     *
     * @return the idx seq
     */
    @Column(name = "IDX_SEQ")
    public Integer getIdxSeq() {
        return idxSeq;
    }

    /**
     * Sets the idx seq.
     *
     * @param idxSeq
     *            the new idx seq
     */
    public void setIdxSeq(final Integer idxSeq) {
        this.idxSeq = idxSeq;
    }

    /**
     * Gets the progress recid.
     *
     * @return the progress recid
     */
    @Column(name = "PROGRESS_RECID")
    public Integer getProgressRecid() {
        return progressRecid;
    }

    /**
     * Sets the progress recid.
     *
     * @param progressRecid
     *            the new progress recid
     */
    public void setProgressRecid(final Integer progressRecid) {
        this.progressRecid = progressRecid;
    }

    /**
     * Gets the doc id.
     *
     * @return the doc id
     */
    @JsonProperty
    @Column(name = "DOC_ID",
            length = COLUMN_LENGTH_50)
    public String getDocId() {
        return docId;
    }

    /**
     * Sets the doc id.
     *
     * @param docId
     *            the new doc id
     */
    public void setDocId(final String docId) {
        this.docId = docId;
    }

    /**
     * Gets the doc req desc.
     *
     * @return the doc req desc
     */
    @JsonProperty
    @Column(name = "DOC_REQ_DESC",
            length = COLUMN_LENGTH_200)
    public String getDocReqDesc() {
        return docReqDesc;
    }

    /**
     * Sets the doc req desc.
     *
     * @param docReqDesc
     *            the new doc req desc
     */
    public void setDocReqDesc(final String docReqDesc) {
        this.docReqDesc = docReqDesc;
    }

    /**
     * Gets the doc client id.
     *
     * @return the doc client id
     */
    @Column(name = "DOC_CLIENT_ID",
            length = COLUMN_LENGTH_50)
    public String getDocClientId() {
        return docClientId;
    }

    /**
     * Sets the doc client id.
     *
     * @param docClientId
     *            the new doc client id
     */
    public void setDocClientId(final String docClientId) {
        this.docClientId = docClientId;
    }

    /**
     * Gets the doc pdf form send.
     *
     * @return the doc pdf form send
     */
    @Column(name = "DOC_PDF_FORM_SEND",
            length = COLUMN_LENGTH_50)
    public String getDocPdfFormSend() {
        return docPdfFormSend;
    }

    /**
     * Sets the doc pdf form send.
     *
     * @param docPdfFormSend
     *            the new doc pdf form send
     */
    public void setDocPdfFormSend(final String docPdfFormSend) {
        this.docPdfFormSend = docPdfFormSend;
    }

    /**
     * Gets the last name.
     *
     * @return the last name
     */
    @Column(name = "LAST_NAME",
            length = COLUMN_LENGTH_40)
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name.
     *
     * @param lastName
     *            the new last name
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the first name.
     *
     * @return the first name
     */
    @Column(name = "FIRST_NAME",
            length = COLUMN_LENGTH_40)
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name.
     *
     * @param firstName
     *            the new first name
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the recipient.
     *
     * @return the recipient
     */
    @Column(name = "RECIPIENT",
            length = COLUMN_LENGTH_40)
    public String getRecipient() {
        return recipient;
    }

    /**
     * Sets the recipient.
     *
     * @param recipient
     *            the new recipient
     */
    public void setRecipient(final String recipient) {
        this.recipient = recipient;
    }

    /**
     * Gets the claim number.
     *
     * @return the claim number
     */
    @Column(name = "CLAIM_NUMBER",
            length = COLUMN_LENGTH_25)
    public String getClaimNumber() {
        return claimNumber;
    }

    /**
     * Sets the claim number.
     *
     * @param claimNumber
     *            the new claim number
     */
    public void setClaimNumber(final String claimNumber) {
        this.claimNumber = claimNumber;
    }

    /**
     * Gets the intake number.
     *
     * @return the intake number
     */
    @Column(name = "INTAKE_NUMBER",
            length = COLUMN_LENGTH_60)
    public String getIntakeNumber() {
        return intakeNumber;
    }

    /**
     * Sets the intake number.
     *
     * @param intakeNumber
     *            the new intake number
     */
    public void setIntakeNumber(final String intakeNumber) {
        this.intakeNumber = intakeNumber;
    }

    /**
     * Gets the matrix client id.
     *
     * @return the matrix client id
     */
    @Column(name = "MATRIX_CLIENT_ID")
    public Integer getMatrixClientId() {
        return matrixClientId;
    }

    /**
     * Sets the matrix client id.
     *
     * @param matrixClientId
     *            the new matrix client id
     */
    public void setMatrixClientId(final Integer matrixClientId) {
        this.matrixClientId = matrixClientId;
    }

    /**
     * Gets the data group.
     *
     * @return the data group
     */
    @Column(name = "DATA_GROUP",
            length = COLUMN_LENGTH_50)
    public String getDataGroup() {
        return dataGroup;
    }

    /**
     * Sets the data group.
     *
     * @param dataGroup
     *            the new data group
     */
    public void setDataGroup(final String dataGroup) {
        this.dataGroup = dataGroup;
    }

    /**
     * Gets the folder.
     *
     * @return the folder
     */
    @Column(name = "FOLDER",
            length = COLUMN_LENGTH_50)
    public String getFolder() {
        return folder;
    }

    /**
     * Sets the folder.
     *
     * @param folder
     *            the new folder
     */
    public void setFolder(final String folder) {
        this.folder = folder;
    }

    /**
     * Gets the doc request id.
     *
     * @return the doc request id
     */
    @Column(name = "DOC_REQUEST_ID",
            length = COLUMN_LENGTH_50)
    public String getDocRequestId() {
        return docRequestId;
    }

    /**
     * Sets the doc request id.
     *
     * @param docRequestId
     *            the new doc request id
     */
    public void setDocRequestId(final String docRequestId) {
        this.docRequestId = docRequestId;
    }

    /**
     * Gets the requested on.
     *
     * @return the requested on
     */
    @Column(name = "REQUESTED_ON")
    public Date getRequestedOn() {
        return requestedOn;
    }

    /**
     * Sets the requested on.
     *
     * @param requestedOn
     *            the new requested on
     */
    public void setRequestedOn(final Date requestedOn) {
        this.requestedOn = requestedOn;
    }

    /**
     * Gets the requested user.
     *
     * @return the requested user
     */
    @Column(name = "REQUESTED_USER",
            length = COLUMN_LENGTH_10)
    public String getRequestedUser() {
        return requestedUser;
    }

    /**
     * Sets the requested user.
     *
     * @param requestedUser
     *            the new requested user
     */
    public void setRequestedUser(final String requestedUser) {
        this.requestedUser = requestedUser;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    @Column(name = "ACTION",
            length = COLUMN_LENGTH_10)
    public String getAction() {
        return action;
    }

    /**
     * Sets the action.
     *
     * @param action
     *            the new action
     */
    public void setAction(final String action) {
        this.action = action;
    }

    /**
     * Gets the exfield 1.
     *
     * @return the exfield 1
     */
    @Column(name = "EXFIELD1",
            length = COLUMN_LENGTH_100)
    public String getExfield1() {
        return exfield1;
    }

    /**
     * Sets the exfield 1.
     *
     * @param exfield1
     *            the new exfield 1
     */
    public void setExfield1(final String exfield1) {
        this.exfield1 = exfield1;
    }

    /**
     * Gets the exfield 2.
     *
     * @return the exfield 2
     */
    @Column(name = "EXFIELD2",
            length = COLUMN_LENGTH_100)
    public String getExfield2() {
        return exfield2;
    }

    /**
     * Sets the exfield 2.
     *
     * @param exfield2
     *            the new exfield 2
     */
    public void setExfield2(final String exfield2) {
        this.exfield2 = exfield2;
    }

    /**
     * Gets the record created date.
     *
     * @return the record created date
     */
    @Column(name = "RECORD_CREATED_DATE")
    public Date getRecordCreatedDate() {
        return recordCreatedDate;
    }

    /**
     * Sets the record created date.
     *
     * @param recordCreatedDate
     *            the new record created date
     */
    public void setRecordCreatedDate(final Date recordCreatedDate) {
        this.recordCreatedDate = recordCreatedDate;
    }

    /**
     * Gets the record updated date.
     *
     * @return the record updated date
     */
    @Column(name = "RECORD_UPDATED_DATE")
    public Date getRecordUpdatedDate() {
        return recordUpdatedDate;
    }

    /**
     * Sets the record updated date.
     *
     * @param recordUpdatedDate
     *            the new record updated date
     */
    public void setRecordUpdatedDate(final Date recordUpdatedDate) {
        this.recordUpdatedDate = recordUpdatedDate;
    }

    /**
     * Gets the java doc nfs reorg done flag.
     *
     * @return the java doc nfs reorg done flag
     */
    @Column(name = "JAVA_DOC_NFS_REORG_DONE_FLAG",
            length = 1)
    public String getJavaDocNfsReorgDoneFlag() {
        return javaDocNfsReorgDoneFlag;
    }

    /**
     * Sets the java doc nfs reorg done flag.
     *
     * @param javaDocNfsReorgDoneFlag
     *            the new java doc nfs reorg done flag
     */
    public void setJavaDocNfsReorgDoneFlag(final String javaDocNfsReorgDoneFlag) {
        this.javaDocNfsReorgDoneFlag = javaDocNfsReorgDoneFlag;
    }

    /**
     * Gets the java doc nfs file location.
     *
     * @return the java doc nfs file location
     */
    @Column(name = "JAVA_DOC_NFS_FILE_LOCATION",
            length = COLUMN_LENGTH_100)
    public String getJavaDocNfsFileLocation() {
        return javaDocNfsFileLocation;
    }

    /**
     * Sets the java doc nfs file location.
     *
     * @param javaDocNfsFileLocation
     *            the new java doc nfs file location
     */
    public void setJavaDocNfsFileLocation(final String javaDocNfsFileLocation) {
        this.javaDocNfsFileLocation = javaDocNfsFileLocation;
    }

    /**
     * Gets the java reorg process comments.
     *
     * @return the java reorg process comments
     */
    @Column(name = "JAVA_REORG_PROCESS_COMMENTS",
            length = COLUMN_LENGTH_200)
    public String getJavaReorgProcessComments() {
        return javaReorgProcessComments;
    }

    /**
     * Sets the java reorg process comments.
     *
     * @param javaReorgProcessComments
     *            the new java reorg process comments
     */
    public void setJavaReorgProcessComments(final String javaReorgProcessComments) {
        this.javaReorgProcessComments = javaReorgProcessComments;
    }
}