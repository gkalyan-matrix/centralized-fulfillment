package com.matrixcos.backendprocess.cf.model.jpa;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PacsDocumentsMetadata.class)
public abstract class PacsDocumentsMetadata_ {

	public static volatile SingularAttribute<PacsDocumentsMetadata, String> docPdfFormSend;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> lastName;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> docId;
	public static volatile SingularAttribute<PacsDocumentsMetadata, Integer> idxSeq;
	public static volatile SingularAttribute<PacsDocumentsMetadata, Date> recordCreatedDate;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> documentFileName;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> intakeNumber;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> docReqDesc;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> action;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> javaReorgProcessComments;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> claimNumber;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> javaDocNfsReorgDoneFlag;
	public static volatile SingularAttribute<PacsDocumentsMetadata, Date> recordUpdatedDate;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> exfield2;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> exfield1;
	public static volatile SingularAttribute<PacsDocumentsMetadata, Integer> progressRecid;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> javaDocNfsFileLocation;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> firstName;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> dataGroup;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> docRequestId;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> folder;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> requestedUser;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> recipient;
	public static volatile SingularAttribute<PacsDocumentsMetadata, Date> requestedOn;
	public static volatile SingularAttribute<PacsDocumentsMetadata, String> docClientId;
	public static volatile SingularAttribute<PacsDocumentsMetadata, Integer> matrixClientId;

	public static final String DOC_PDF_FORM_SEND = "docPdfFormSend";
	public static final String LAST_NAME = "lastName";
	public static final String DOC_ID = "docId";
	public static final String IDX_SEQ = "idxSeq";
	public static final String RECORD_CREATED_DATE = "recordCreatedDate";
	public static final String DOCUMENT_FILE_NAME = "documentFileName";
	public static final String INTAKE_NUMBER = "intakeNumber";
	public static final String DOC_REQ_DESC = "docReqDesc";
	public static final String ACTION = "action";
	public static final String JAVA_REORG_PROCESS_COMMENTS = "javaReorgProcessComments";
	public static final String CLAIM_NUMBER = "claimNumber";
	public static final String JAVA_DOC_NFS_REORG_DONE_FLAG = "javaDocNfsReorgDoneFlag";
	public static final String RECORD_UPDATED_DATE = "recordUpdatedDate";
	public static final String EXFIELD2 = "exfield2";
	public static final String EXFIELD1 = "exfield1";
	public static final String PROGRESS_RECID = "progressRecid";
	public static final String JAVA_DOC_NFS_FILE_LOCATION = "javaDocNfsFileLocation";
	public static final String FIRST_NAME = "firstName";
	public static final String DATA_GROUP = "dataGroup";
	public static final String DOC_REQUEST_ID = "docRequestId";
	public static final String FOLDER = "folder";
	public static final String REQUESTED_USER = "requestedUser";
	public static final String RECIPIENT = "recipient";
	public static final String REQUESTED_ON = "requestedOn";
	public static final String DOC_CLIENT_ID = "docClientId";
	public static final String MATRIX_CLIENT_ID = "matrixClientId";

}

