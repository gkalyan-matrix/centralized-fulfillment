/*
 * Copyright (c) 2020, Matrix Absence Management, Inc. All rights reserved.
 * 
 */
package com.matrixcos.backendprocess.cf;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.matrixcos.backendprocess.cf.model.jpa.PacsDocumentsMetadata;
import com.matrixcos.backendprocess.cf.service.CentralizedFulfillmentService;

/**
 * The Class CentralizedFulfillment.
 */
// SUPPRESS CHECKSTYLE 1
@Configuration
public class CentralizedFulfillment {

    /** The logger. */
    private Logger logger = ESAPI.getLogger(CentralizedFulfillment.class);

    /** The context. */
    private static ApplicationContext context;

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    // SUPPRESS CHECKSTYLE 1
    public static void main(String[] args) {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
        final CentralizedFulfillment centralizedFulfillmentMain = context.getBean(CentralizedFulfillment.class);
        centralizedFulfillmentMain.start();
    }

    /** The continuous fulfillment service. */
    @Autowired
    private CentralizedFulfillmentService cfService;

    /**
     * Start.
     */
    private void start() {
        try {
            final List<PacsDocumentsMetadata> listOfDocuments = cfService.getPacsDocumentsMetadataByRecordCreatedDate(yesterday());
            logger.info(Logger.EVENT_SUCCESS, "" + listOfDocuments.size());
            if (listOfDocuments.size() > 0) {
                for (PacsDocumentsMetadata document : listOfDocuments) {
                    logger.info(Logger.EVENT_SUCCESS, document.getDocumentFileName());
                }
            }
        } catch (final Exception ex) {
            logger.info(Logger.EVENT_FAILURE, "Exception=" + ex.getMessage());
        }
    }
    
    /**
     * Yesterday.
     *
     * @return the date
     */
    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }
}
