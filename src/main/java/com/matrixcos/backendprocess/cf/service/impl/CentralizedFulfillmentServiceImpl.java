/*
 * Copyright (c) 2020, Matrix Absence Management, Inc. All rights reserved.
 * 
 */
package com.matrixcos.backendprocess.cf.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.stereotype.Service;

import com.matrixcos.backendprocess.cf.model.jpa.PacsDocumentsMetadata;
import com.matrixcos.backendprocess.cf.model.jpa.PacsDocumentsMetadata_;
import com.matrixcos.backendprocess.cf.service.CentralizedFulfillmentService;
import com.matrixcos.db.lib.service.PersistenceService;

/**
 * The Class CentralizedFulfillmentServiceImpl.
 */
@Service
public class CentralizedFulfillmentServiceImpl extends PersistenceService implements CentralizedFulfillmentService {

    /** The logger. */
    protected static Logger logger = ESAPI.getLogger(CentralizedFulfillmentServiceImpl.class);
    
    /*
     * (non-Javadoc)
     * 
     * @see com.matrixcos.backendprocess.cf.service.CentralizedFulfillmentService#getPacsDocumentsMetadataByRecordCreatedDate(java.lang.Long)
     */
    @Override
    public List<PacsDocumentsMetadata> getPacsDocumentsMetadataByRecordCreatedDate(final Date recordCreatedDate) {
        final CriteriaBuilder cb = getCriteriaBuilder();
        final CriteriaQuery<PacsDocumentsMetadata> criteriaQuery = cb.createQuery(PacsDocumentsMetadata.class);
        final Root<PacsDocumentsMetadata> rootPacsDocument = criteriaQuery.from(PacsDocumentsMetadata.class);
        final List<Predicate> predicates = new ArrayList<Predicate>();
        criteriaQuery.multiselect(rootPacsDocument.get(PacsDocumentsMetadata_.docReqDesc), rootPacsDocument.get(PacsDocumentsMetadata_.docId), rootPacsDocument.get(PacsDocumentsMetadata_.documentFileName),
                rootPacsDocument.get(PacsDocumentsMetadata_.recordCreatedDate));
        predicates.add(cb.greaterThan(rootPacsDocument.<Date>get(PacsDocumentsMetadata_.recordCreatedDate), recordCreatedDate));
        criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        return createQuery(criteriaQuery).getResultList();
    }
}
