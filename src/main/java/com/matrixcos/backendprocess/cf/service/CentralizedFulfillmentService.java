/*
 * Copyright (c) 2020, Matrix Absence Management, Inc. All rights reserved.
 * 
 */
package com.matrixcos.backendprocess.cf.service;

import java.util.Date;
import java.util.List;

import com.matrixcos.backendprocess.cf.model.jpa.PacsDocumentsMetadata;

/**
 * The Interface CentralizedFulfillmentService.
 */
public interface CentralizedFulfillmentService {

    /**
     * Gets the pacs documents metadata by record created date.
     *
     * @param recordCreatedDate
     *            the record created date
     * @return the pacs documents metadata by record created date
     */
    List<PacsDocumentsMetadata> getPacsDocumentsMetadataByRecordCreatedDate(Date recordCreatedDate);
    
}
